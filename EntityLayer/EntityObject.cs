﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public abstract class EntityObject
    {
        public int ID { get; }
        
        public EntityObject(int EntityID){
            ID = EntityID;
        } 
    }
}
