﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/** TODO
 * Method removeHouser
 */
namespace EntityLayer
{
    public class House : EntityObject
    {
        //Attributes
        public List<Character> housers;
        public string name;
        public int numberOfUnities;

        //Constructor
        public House(int ID, String houseName):base(ID)
        {
            name = houseName;
            housers = new List<Character>();
            numberOfUnities = 0;
        }

        public House(int ID, string houseName, int num):base(ID)
        {
            name = houseName;
            numberOfUnities = num;
        }

        //Methods
        public void addHouser(Character newHouser)
        {
            housers.Add(newHouser);
            numberOfUnities++;
        }

        public int getNumberOfUnities()
        {
            return numberOfUnities;
        }

        //Getters
        public string getName()
        {
            return name;
        }
    }
}
