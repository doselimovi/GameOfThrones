﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class Territory : EntityObject
    {
        //Attributes
        public TerritoryTypeEnum territoryType;
        public int houseOwner;

        //Constructor
        public Territory(int ID, TerritoryTypeEnum type, int house):base(ID)
        {
            territoryType = type;
            houseOwner = house;
        }

        //Methods
        public string toString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(territoryType).Append(";").Append(houseOwner);

            return builder.ToString();
        }

        //GETTERS
        public int getTerritoryType()
        {
            return (int)territoryType;
        }

        public int getHouseOwner()
        {
            return houseOwner;
        }
    
    }

    public enum TerritoryTypeEnum
    {
        SEA,
        MOUNTAIN,
        LAND,
        DESERT
    }
}
