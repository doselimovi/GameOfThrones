﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class Fight : EntityObject
    {
        //Attributes
        public int houseChallenger1;
        public int houseChallenger2;
        public int winningHouse; //0 si aucun, 1 ou 2 sinon

        //Constructor
        public Fight(int ID, int house1, int house2):base(ID)
        {
            houseChallenger1 = house1;
            houseChallenger2 = house2;
            winningHouse = 0;
        }

        //Methods
        public string toString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(houseChallenger1).Append(";").Append(houseChallenger2);

            return builder.ToString();
        }

        //Getters
        public int getHouseChallenger1()
        {
            return houseChallenger1;
        }

        public int getHouseChallenger2()
        {
            return houseChallenger2;
        }
    }
}
