﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class War : EntityObject
    {
        //Attributes
        public List<Fight> fights;

        //Constructor
        public War(int ID, List<Fight> warFights):base(ID)
        {
            fights = warFights;
        }
    }
}
