﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class Duel
    {
        public int id { get; set; }
        public Character character1 { get; set; }
        public Character character2 { get; set; }
        public float cote1 { get; set; }
        public float cote2 { get; set; }

        public Duel(int id,  Character character1, Character character2)
        {
            this.id = id;
            this.character1 = character1;
            this.character2 = character2;
            cote1 = 0;
            cote2 = 0;
        }

        public void calculCotes()
        {
            int chance1 = calculChance(character1);
            int chance2 = calculChance(character2);

            // calcule la cote entre 1 et 2
            this.cote1 = ((chance2 - chance1 + 30000) / 60000.0f) + 1.0f;
            this.cote2 = ((chance1 - chance2 + 30000) / 60000.0f) + 1.0f;
        }

        public int calculChance(Character c)
        {
            return (c.bravoury + c.crazyness + c.strength) * c.health;
        }

        public Character calculGagnant()
        {
            Random rand = new Random();

            // lowerCote représente la cote du plus probable gagnant (celui avec la cote la plus faible)
            // et on part du principe que c'est lui qui gagne
            double lowerCote = cote1;
            Character winner = character1;
            if (cote2 < lowerCote)
            {
                lowerCote = cote2;
                winner = character2;
            }

            // on fait de l'aléatoire pour savoir si le plus probable gagant a été battu
            // si oui on met à jour le gagnant
            // (on fait comme ça parce qu'on part du principe que 1 < cote < 2
            if (rand.NextDouble() + 1.0 < lowerCote)
            {
                winner = (lowerCote == cote1) ? character2 : character1;
            }

            return winner;
        }
    }
}
