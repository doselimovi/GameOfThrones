﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class Character : EntityObject
    {
        //Attributes
        public int bravoury; //Over 100
        public int crazyness; //Over 100
        public int strength; //Over 100
        public string firstName;
        public string lastName;
        public int health; //Over 100
        public Dictionary<int, RelationshipEnum> relationships;
        
        //Constructor
        public Character(int ID, String firstn, String lastn) : base(ID)
        {
            bravoury = 50;
            crazyness = 0;
            strength = 25;
            firstName = firstn;
            lastName = lastn;
            health = 100;
            relationships = new Dictionary<int, RelationshipEnum>();
        }

        public Character(int ID, String firstn, String lastn, int brav, int craz, int stren, int heal) : base(ID)
        {
            firstName = firstn;
            lastName = lastn;
            bravoury = brav;
            crazyness = craz;
            strength = stren;
            health = heal;
            relationships = new Dictionary<int, RelationshipEnum>();
        }

        //Methods
        public String toString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(ID).Append(";").Append(firstName).Append(";").Append(lastName).Append(";").Append(bravoury).Append(";").Append(crazyness).Append(";").Append(strength).Append(";").Append(health);

            return builder.ToString();
        }

        public void addRelation(int ID, RelationshipEnum type) {
            relationships.Add(ID, type);
        }

        //Getters
        public int getStrength()
        {
            return strength;
        }

        public int getHealth()
        {
            return health;
        }

        public string getFirstName()
        {
            return firstName;
        }

        public string getLastName()
        {
            return lastName;
        }

        public string getFullName()
        {
            return firstName + " " + lastName;
        }

   
    }

    public enum RelationshipEnum
    {
        FRIENDSHIP,
        LOVE,
        HATRED,
        RIVALRY
    }

    public enum CharacterTypeEnum
    {
        WARRIOR,
        WITCH,
        TACTICIAN,
        LEADER,
        LOSER
    }

   
}
