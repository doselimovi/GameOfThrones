﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;

namespace WebApplication.Models
{
    public class CharacterModel
    {
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }
        public int Strength { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Health { get; set; }
        public Dictionary<int, RelationshipEnum> Relationships { get; set; }
        public CharacterModel(Character c)
        {
            Bravoury = c.bravoury;
            Crazyness = c.crazyness;
            Strength = c.strength;
            FirstName = c.firstName;
            LastName = c.lastName;
            Health = c.health;
            Relationships = c.relationships;
        }
    }
}