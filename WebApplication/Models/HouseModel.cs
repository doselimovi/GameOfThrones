﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;

namespace WebApplication.Models
{
    public class HouseModel
    {
        public int NumberOfUnits { get; set; }
        public string Name { get; set; }
        public List<Character> Housers { get; set; }
        public HouseModel(House h)
        {
            NumberOfUnits = h.numberOfUnities;
            Name = h.name;
            Housers = h.housers;
        }
    }
}