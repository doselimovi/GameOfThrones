﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;

namespace WebApplication.Models
{
    public class TerritoryModel
    {
        public TerritoryTypeEnum TerritoryType { get; set; }
        public int HouseOwner { get; set; }
        public TerritoryModel(Territory t)
        {
            TerritoryType = t.territoryType;
            HouseOwner = t.houseOwner;
        }
    }
}