﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;

namespace WebApplication.Models
{
    public class DuelModel
    {
        public int Id { get; set; }
        public Character Character1 { get; set; }
        public Character Character2 { get; set; }
        public float Cote1 { get; set; }
        public float Cote2 { get; set; }

        public DuelModel(Duel d)
        {
            Id = d.id;
            Character1 = d.character1;
            Character2 = d.character2;
            Cote1 = d.cote1;
            Cote2 = d.cote2;
        }
    }
}