﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;

namespace WebApplication.Models
{
    public class FightModel
    {
        public int HouseChallenger1 { get; set; }
        public int HouseChallenger2 { get; set; }
        public int WinningHouse { get; set; }

        public FightModel(Fight f)
        {
            HouseChallenger1 = f.houseChallenger1;
            HouseChallenger2 = f.houseChallenger2;
            WinningHouse = f.winningHouse;
        }
    }
}