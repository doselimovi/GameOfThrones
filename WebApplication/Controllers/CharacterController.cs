﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EntityLayer;
using WebApplication.Models;
using Newtonsoft.Json.Linq;

namespace WebApplication.Controllers
{
    public class CharacterController : Controller
    {
        //
        // GET: /Character/

        public async Task<ActionResult> Index()
        {
            List<CharacterModel> Characters = new List<CharacterModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4205/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                // New Code:
                HttpResponseMessage response = await client.GetAsync("Character/Index");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    var obj = JsonConvert.DeserializeObject(temp);
                    System.Diagnostics.Debug.WriteLine(temp);
                    int i = 0;
                    foreach(var item in ((JArray)obj))
                    {
                        Character c = new Character(i, item.Value<String>("FirstName"), item.Value<String>("LastName"), item.Value<int>("Bravoury"), item.Value<int>("Crazyness"), item.Value<int>("Strength"), item.Value<int>("Health"));
                        Characters.Add(new CharacterModel(c));
                        i++;
                    }
                    /*
                    List<Character> listTMP = JsonConvert.DeserializeObject<List<Character>>(temp);
                    foreach (Character h in listTMP)
                    {
                        Characters.Add(new CharacterModel(h));
                    }
                    */
                }
            }

            return View(Characters);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            //TO DO
            return RedirectToAction("Index");
        }

        public ActionResult Edit()
        {
            return View();
        }


    }
}