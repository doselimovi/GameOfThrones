﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EntityLayer;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HouseController : Controller
    {
        //
        // GET: /House/

        public async Task<ActionResult> Index()
        {
            List<HouseModel> Houses = new List<HouseModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4205/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                // New Code:
                HttpResponseMessage response = await client.GetAsync("api/houses");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    List<House> listTMP = JsonConvert.DeserializeObject<List<House>>(temp);
                    foreach(House h in listTMP)
                    {
                        Houses.Add(new HouseModel(h));
                    }
                }
            }
            
            return View(Houses);
        }


    }
}
