﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EntityLayer;
using WebApplication.Models;
using Newtonsoft.Json.Linq;

namespace WebApplication.Controllers
{
    public class DuelController : Controller
    {
        // GET: Duel
        public async Task<ActionResult> Index()
        {

            List<DuelModel> Duels = new List<DuelModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4205/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                // New Code:
                HttpResponseMessage response = await client.GetAsync("Duel/Index");
                if (response.IsSuccessStatusCode)
                {
                    string text = await response.Content.ReadAsStringAsync();
                    var arr = JArray.Parse(text);
                    foreach (var duelObj in arr)
                    {
                        var c1Obj = duelObj.SelectToken("id1");
                        Character c1 = new Character(
                            (int) c1Obj.SelectToken("ID"),
                            (string) c1Obj.SelectToken("firstName"),
                            (string) c1Obj.SelectToken("lastName"),
                            (int) c1Obj.SelectToken("bravoury"),
                            (int) c1Obj.SelectToken("crazyness"),
                            (int) c1Obj.SelectToken("strength"),
                            (int) c1Obj.SelectToken("health")
                        );

                        var c2Obj = duelObj.SelectToken("id2");
                        Character c2 = new Character(
                            (int) c2Obj.SelectToken("ID"),
                            (string) c2Obj.SelectToken("firstName"),
                            (string) c2Obj.SelectToken("lastName"),
                            (int) c2Obj.SelectToken("bravoury"),
                            (int) c2Obj.SelectToken("crazyness"),
                            (int) c2Obj.SelectToken("strength"),
                            (int) c2Obj.SelectToken("health")
                        );
                        
                        Duel d = new Duel((int)duelObj.SelectToken("id"), c1, c2);
                        d.cote1 = (float) duelObj.SelectToken("cote1");
                        d.cote2 = (float) duelObj.SelectToken("cote2");

                        Duels.Add(new DuelModel(d));
                    }
                }
            }

            return View(Duels);
        }
    }
}