﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EntityLayer;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class TerritoryController : Controller
    {
        // GET: Territory
        public async Task<ActionResult> Index()
        {
            List<TerritoryModel> Territories = new List<TerritoryModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4205/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                // New Code:
                HttpResponseMessage response = await client.GetAsync("api/territory");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    List<Territory> listTMP = JsonConvert.DeserializeObject<List<Territory>>(temp);
                    foreach (Territory t in listTMP)
                    {
                        Territories.Add(new TerritoryModel(t));
                    }
                }
            }

            return View(Territories);
        }
    }
}