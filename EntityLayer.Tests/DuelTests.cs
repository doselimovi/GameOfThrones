﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Tests
{
    [TestClass()]
    public class DuelTests
    {
        [TestMethod()]
        public void calculChanceTest()
        {
            Character c1 = new Character(0, "", "", 1, 1, 1, 0);
            Character c2 = new Character(1, "", "", 100, 100, 100, 100);
            Duel duel = new Duel(c1, c2);

            Assert.AreEqual(0, duel.calculChance(c1));
            Assert.AreEqual(30000, duel.calculChance(c2));
        }

        [TestMethod()]
        public void calculCotesTest()
        {
            Character c1 = new Character(0, "", "", 1, 1, 1, 0);
            Character c2 = new Character(1, "", "", 100, 100, 100, 100);
            Duel duel = new Duel(c1, c2);

            Assert.AreEqual(0, duel.cote1);
            Assert.AreEqual(0, duel.cote2);

            duel.calculCotes();

            if (duel.cote1 < 1 || duel.cote1 > 2)
            {
                Assert.Fail($"La cote soit être entre 1 et 2 (cote1={duel.cote1})");
            }

            if (duel.cote2 < 1 || duel.cote2 > 2)
            {
                Assert.Fail($"La cote soit être entre 1 et 2 (cote1={duel.cote2})");
            }

            Assert.AreEqual(2, duel.cote1);
            Assert.AreEqual(1, duel.cote2);
        }
    }
}