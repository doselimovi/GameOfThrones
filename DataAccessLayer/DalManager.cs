﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using EntityLayer;

namespace DataAcessLayer
{
    //Explorateur de serveurs > C.D. sur connexions de données > Ajouter > fichier


    public class DalManager : DataManager
    {
        private static DalManager instance = null;

        String connectionString;

        private DalManager()
        {
            connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=H:\Cours\servicesWeb\GameOfThrones\ThronesDB\ThronesDB.mdf;Integrated Security=True;Connect Timeout=30";
        }

        public static DalManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DalManager();
                }
                return instance;
            }
        }


        /** --- CHARACTERS --- **/

        public List<Character> getCharacters()
        {
            List<Character> characters = new List<Character>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT * FROM Characters", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    Character c = new Character(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetInt32(5), reader.GetInt32(6));
                    characters.Add(c);
                }
                conn.Close();
            }

            return characters;
        }

        public Character getCharacterById(int id)
        {
            Character character;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"SELECT * FROM Characters WHERE ID={id}", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                if (!reader.Read())
                {
                    return null;
                }

                character = new Character(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetInt32(5), reader.GetInt32(6));
                conn.Close();
            }

            return character;
        }


        public Character newCharacter(string firstName, string lastName)
        {
            Character c = null;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"INSERT INTO Characters(firstname, lastname) OUTPUT INSERTED.Id VALUES('{firstName}', '{lastName}')", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                if (reader.Read())
                {
                    c = new Character(reader.GetInt32(0), firstName, lastName);
                }

                conn.Close();
            }

            return c;
        }

        public void deleteCharacterById(int ID)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"DELETE FROM Characters WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }
        }

        public void updateCharacter(Character c)
        {
            if (c.bravoury <= 100 && c.bravoury >= 0 && c.crazyness <= 100 && c.crazyness >= 0 && c.strength <= 100 && c.strength >= 0 && c.health <= 100 && c.health >= 0)
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand comm = new SqlCommand($"UPDATE Characters SET bravoury='{c.bravoury}', crazyness='{c.crazyness}', strength='{c.strength}', health='{c.health}', firstname='{c.firstName}', lastname='{c.lastName} WHERE Id='{c.ID}'", conn);
                    conn.Open();

                    SqlDataReader reader = comm.ExecuteReader();
                    conn.Close();
                }
            }
        }


        public void setBravoury(int ID, int bravoury)
        {
            if(bravoury <= 100 && bravoury >= 0)
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {                   
                    SqlCommand comm = new SqlCommand($"UPDATE Characters SET bravoury='{bravoury}' WHERE Id='{ID}'", conn);
                    conn.Open();

                    SqlDataReader reader = comm.ExecuteReader();
                    conn.Close();
                }
            }
        }

        public void setCrazyness(int ID, int crazyness)
        {
            if (crazyness <= 100 && crazyness >= 0)
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand comm = new SqlCommand($"UPDATE Characters SET crazyness='{crazyness}' WHERE Id='{ID}'", conn);
                    conn.Open();

                    SqlDataReader reader = comm.ExecuteReader();
                    conn.Close();
                }
            }
        }

        public void setStrength(int ID, int strength)
        {
            if (strength <= 100 && strength >= 0)
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand comm = new SqlCommand($"UPDATE Characters SET strength='{strength}' WHERE Id='{ID}'", conn);
                    conn.Open();

                    SqlDataReader reader = comm.ExecuteReader();
                    conn.Close();
                }
            }
        }

        public void setHealth(int ID, int health)
        {
            if (health <= 100 && health >= 0)
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand comm = new SqlCommand($"UPDATE Characters SET health='{health}' WHERE Id='{ID}'", conn);
                    conn.Open();

                    SqlDataReader reader = comm.ExecuteReader();
                    conn.Close();
                }
            }
        }

        public void setFistname(int ID, string firstname)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Characters SET firstname='{firstname}' WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }
            
        }

        public void setlastname(int ID, string lastname)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Characters SET lastname='{lastname}' WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }

        }


        /** --- HOUSES --- **/
        public List<House> getHouses()
        {
            List<House> houses = new List<House>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT * FROM Houses", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    House h = new House(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
                    houses.Add(h);
                }
                conn.Close();
            }

            return houses;
        }

        public House getHouseById(int id)
        {
            House house;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"SELECT * FROM Houses WHERE ID={id}", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                if (!reader.Read())
                {
                    return null;
                }
                house = new House(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
                conn.Close();
            }

            return house;
        }

        public List<House> getHousesOver(int nb)
        {
            List<House> houses = new List<House>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT * FROM Houses", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    House h = new House(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
                    if (h.getNumberOfUnities() > nb)
                    {
                        houses.Add(h);
                    }
                }
                conn.Close();
            }

            return houses;
        }

        public House newHouse(string name, int numberOfUnits)
        {
            House h = null;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"INSERT INTO Houses(name, numberofentities)  OUTPUT INSERTED.Id VALUES('{name}', '{numberOfUnits}')", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                if (reader.Read())
                {
                    h = new House(reader.GetInt32(0), name);
                }
               
                conn.Close();
            }

            return h;
        }


        public void deleteHouseById(int ID)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"DELETE FROM Houses WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }
        }

        public void updateHouse(House h)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Houses SET name='{h.name}', numberofentities='{h.numberOfUnities}' WHERE Id='{h.ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                conn.Close();
            }
        }

        public void setHouseName(int ID, string houseName)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Houses SET name='{houseName}' WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                conn.Close();
            }

        }

        public void setHouseNumberOfUnits(int ID, int nbOfUnits)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Houses SET numberofentities='{nbOfUnits}' WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                conn.Close();
            }
        }



        /** --- TERRITORIES --- **/

        public List<Territory> getTerritories()
        {
            List<Territory> territories = new List<Territory>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT * FROM Territories", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read()) { 

                    Territory t = new Territory(reader.GetInt32(0), (TerritoryTypeEnum)reader.GetInt32(1), reader.GetInt32(2));
                    territories.Add(t);

                }
                conn.Close();
            }

            return territories;
        }


        public Territory getTerritoryById(int id)
        {
            Territory territory;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"SELECT * FROM Territories WHERE ID={id}", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                if (!reader.Read())
                {
                    return null;
                }
                territory = new Territory(reader.GetInt32(0), (TerritoryTypeEnum)reader.GetInt32(1), reader.GetInt32(2));
                conn.Close();
            }

            return territory;
        }


        public Territory newTerritory(Territory t)
        {
            Territory territory = null;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"INSERT INTO Territories(type, ownerId) OUTPUT INSERTED.Id VALUES('{t.getTerritoryType()}', '{t.getHouseOwner()}')", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                if (reader.Read())
                {
                    territory = new Territory(reader.GetInt32(0), (TerritoryTypeEnum)t.getTerritoryType(), t.getHouseOwner());
                }

                conn.Close();
            }
            return territory;
        }


        public void deleteTerritoryById(int ID)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"DELETE FROM Territories WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }
        }

        public void setOwnerId(int ID, int ownerId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Territories SET ownerId='{ownerId}' WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }

        }



        /** --- FIGHTS --- **/

        public List<Fight> getFights()
        {
            List<Fight> fights = new List<Fight>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT * FROM Fights", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {

                    Fight f = new Fight(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2));
                    fights.Add(f);

                }
                
                conn.Close();
            }

            return fights;
        }


        public Fight getFightById(int id)
        {
            Fight fight;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"SELECT * FROM Fights WHERE ID={id}", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                if (!reader.Read())
                {
                    return null;
                }
                fight = new Fight(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2));
                conn.Close();
            }

            return fight;
        }


        public Fight newFight(Fight f)
        {
            Fight fight = null;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"INSERT INTO Fights(house1, house2) OUTPUT INSERTED.Id VALUES('{f.getHouseChallenger1()}', '{f.getHouseChallenger2()}')", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                if (reader.Read())
                {
                    fight = new Fight(reader.GetInt32(0), f.getHouseChallenger1(), f.getHouseChallenger2());
                }

                conn.Close();
            }
            return fight;
        }


        public void deleteFightById(int ID)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"DELETE FROM Fights WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }
        }

        public void updateFight(Fight f)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Fights SET house1='{f.houseChallenger1}', house2='{f.houseChallenger2}', winner='{f.winningHouse}' WHERE Id='{f.ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }
        }

        public void setHouse1(int ID, int house1)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Fights SET house1='{house1}' WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }

        }

        public void setHouse2(int ID, int house2)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Fights SET house2='{house2}' WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }

        }

        public void setWinner(int ID, int winner)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand($"UPDATE Territories SET winner='{winner}' WHERE Id='{ID}'", conn);
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                conn.Close();
            }

        }


    }
}
