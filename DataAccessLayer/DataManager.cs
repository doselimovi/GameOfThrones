﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;

namespace DataAcessLayer
{
    public interface DataManager
    {
        List<Character> getCharacters();
        List<House> getHouses();
        List<House> getHousesOver(int nb);
        List<Territory> getTerritories();
        List<Fight> getFights();
    }
}


