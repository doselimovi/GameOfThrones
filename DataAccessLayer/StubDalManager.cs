﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;

namespace DataAcessLayer
{
    public class StubDalManager : DataManager
    {
        //Attributes
        private List<Character> characters;
        private List<House> houses;
        private List<Territory> territories;
        private List<Fight> fights;

        //Constructor
        public StubDalManager()
        {
            characters = new List<Character>();
            houses = new List<House>();
            territories = new List<Territory>();
            fights = new List<Fight>();

            //Characters
            characters.Add(new Character(0, "Jon", "Snow"));
            characters.Add(new Character(1, "Daenerys", "Targaryen"));
            characters.Add(new Character(2, "Arya", "Stark"));
            characters.Add(new Character(3, "A man has", "No name"));

            //houses
            houses.Add(new House(0, "Stark"));
            houses.Add(new House(1, "Targaryen"));
            houses.Add(new House(2, "Lanister"));

            //territories
            territories.Add(new Territory(0, TerritoryTypeEnum.SEA, 0));
            territories.Add(new Territory(1, TerritoryTypeEnum.MOUNTAIN, 1));
            territories.Add(new Territory(2, TerritoryTypeEnum.MOUNTAIN, 0));
            territories.Add(new Territory(3, TerritoryTypeEnum.LAND, 2));

            //Fights
            fights.Add(new Fight(0, 0, 1));
            fights.Add(new Fight(1, 1, 2));
        }

        //Methods
        public List<Character> getCharacters()
        {
            return characters;
        }

        public List<House> getHouses()
        {
            return houses;
        }

        public List<House> getHousesOver(int nb)
        {
            List<House> returnedHouses = new List<House>();
            foreach(House house in houses)
            {
                if (house.getNumberOfUnities() > nb)
                {
                    returnedHouses.Add(house);
                }
            }

            return returnedHouses;
        }

        public List<Territory> getTerritories()
        {
            return territories;
        }

        public List<Fight> getFights()
        {
            return fights;
        }
    }
}
