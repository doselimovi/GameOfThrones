﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAcessLayer;
using EntityLayer;

namespace BusinessLayer
{


    public class TournamentManager
    {
        private static TournamentManager instance = null;
        public static float solde;

        public List<Duel> duels;

        //Constructor
        private TournamentManager()
        {
        }

        public static TournamentManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TournamentManager();
                    solde = 100;
                    instance.duels = new List<Duel>();
                }
                return instance;
            }
        }

        //Methods

            /** --- FIGHTS --- **/

        public Fight createFight(Fight f)
        {
            return DalManager.Instance.newFight(f);
        }
       
        public IEnumerable<string> getFights()
        {
            List<Fight> listFights = DalManager.Instance.getFights();

            IEnumerable<string> results = listFights.Select(fight => fight.getHouseChallenger1() + " vs " + fight.getHouseChallenger2());

            return results;
        }

        public Fight getFightById(int id)
        {
            return DalManager.Instance.getFightById(id);
        }

        public void deleteFightById(int id)
        {
            DalManager.Instance.deleteFightById(id);
        }

        public void updateFight(Fight f)
        {
            DalManager.Instance.updateFight(f);
        }

        public void setFightHouse1(int id, int house1ID)
        {
            DalManager.Instance.setHouse1(id, house1ID);
        }

        public void setFightHouse2(int id, int house2ID)
        {
            DalManager.Instance.setHouse2(id, house2ID);
        }

        public void setFightWinner(int id, int winnerID)
        {
            DalManager.Instance.setWinner(id, winnerID);
        }


            /** --- HOUSES --- **/

        public House createHouse(string name, int numberOfUnits)
        {
            return DalManager.Instance.newHouse(name, numberOfUnits);
        }

        public IEnumerable<House> getHouses()
        {
            return DalManager.Instance.getHouses();
        }

        public void updateHouse(House h)
        {
            DalManager.Instance.updateHouse(h);
        }

        public House getHouseById(int id)
        {
            return DalManager.Instance.getHouseById(id);
        }

        public void deleteHouseById(int id)
        {
            DalManager.Instance.deleteHouseById(id);
        }

        public void setHouseName(int id, string name)
        {
            DalManager.Instance.setHouseName(id, name);
        }

        public void setHouseNumberOfUnits(int id, int nbOfUnits)
        {
            DalManager.Instance.setHouseNumberOfUnits(id, nbOfUnits);
        }


            /** --- CHARACTERS --- **/
        
        public Character createCharacter(string firstName, string lastName)
        {
            return DalManager.Instance.newCharacter(firstName, lastName);
        }

        public IEnumerable<Character> getCharacters()
        {
            return DalManager.Instance.getCharacters();
        }

        public Character getCharacterById(int id)
        {
            return DalManager.Instance.getCharacterById(id);
        }

        public IEnumerable<string> getCharacterNames(int minStrength, int minHealth)
        {
            List<Character> listCharacters = DalManager.Instance.getCharacters();

            IEnumerable<string> results = listCharacters.Where(character => character.getStrength() > minStrength && character.getHealth() > minHealth).Select(character => character.getFullName());

            return results;
        }

        public void updateCharacter(Character c)
        {
            DalManager.Instance.updateCharacter(c);
        }

        public void deleteCharacterById(int id)
        {
            DalManager.Instance.deleteCharacterById(id);
        }

        public void setCharacterBravoury(int id, int bravoury)
        {
            DalManager.Instance.setBravoury(id, bravoury);
        }

        public void setCharacterCrazyness(int id, int crazyness)
        {
            DalManager.Instance.setCrazyness(id, crazyness);
        }

        public void setCharacterStrength(int id, int strength)
        {
            DalManager.Instance.setStrength(id, strength);
        }

        public void setCharacterHealth(int id, int health)
        {
            DalManager.Instance.setHealth(id, health);
        }
        
        public void setCharacterFirstName(int id, string firstName)
        {
            DalManager.Instance.setFistname(id, firstName);
        }

        public void setCharacterLastName(int id, string lastName)
        {
            DalManager.Instance.setlastname(id, lastName);
        }

            /** --- TERRITORIES --- **/

        public Territory createTerritory(Territory t)
        {
            return DalManager.Instance.newTerritory(t);
        }
        
        public IEnumerable<string> getTerritories()
        {
            List<Territory> listTerritories = DalManager.Instance.getTerritories();

            IEnumerable<string> results = listTerritories.Select(territory => territory.toString());

            return results;
        }

        public Territory getTerritoryById(int id)
        {
            return DalManager.Instance.getTerritoryById(id);
        }

        public void deleteTerritoryById(int id)
        {
            DalManager.Instance.deleteTerritoryById(id);
        }

        public void setTerritoryOwnerId(int id, int ownerId)
        {
            DalManager.Instance.setOwnerId(id, ownerId);
        }

        /** --- DUELS --- **/
        public List<Duel> getNewDuels()
        {
            //Création des duels
            duels = new List<Duel>();
            List<Character> characters = new List<Character>();
            characters = Instance.getCharacters().ToList();
            Random rand = new Random();
            for (int n = 0; n < 5 && characters.Count >= 2; n++)
            {
                System.Diagnostics.Debug.WriteLine(characters.Count);
                int i = rand.Next(0, characters.Count);
                Character c1 = characters[i];
                characters.RemoveAt(i);

                i = rand.Next(0, characters.Count);
                Character c2 = characters[i];
                characters.RemoveAt(i);
                
                duels.Add(new Duel(n, c1, c2));
            }
            
            // calcul des cotes
            foreach (Duel duel in duels)
            {
                duel.calculCotes();
            }

            return duels;
        }

        public float getResultOfDuel(int duelId, int choice, int mise)
        {
            //Appelle calculGagnant() sur le duel
            //Calcule le nouveau solde
            //Renvoie le nouveau solde

            Duel duel = duels[duelId];

            Character winner = duel.calculGagnant();
            float gain = 0;
            if ((winner.getFirstName() == duels[duelId].character1.getFirstName() && choice==1) || (winner.getFirstName() == duels[duelId].character2.getFirstName() && choice == 2))
            {
                gain = mise * ((winner.ID == duel.character1.ID) ? duel.cote1 : duel.cote2);
            }
            else
            {
                gain -= mise;
            }

            solde += gain;

            return solde;
        }

        public float getSolde()
        {
            return solde;
        }
    }
}
