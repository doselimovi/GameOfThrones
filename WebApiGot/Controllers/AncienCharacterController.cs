﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApiGot.Models;
using EntityLayer;

namespace WebApiGot.Controllers
{
    [RoutePrefix("api/characters2")]
    public class AncienCharacterController : ApiController
    {


        [Route("")]
        [HttpGet]
        public List<CharacterDTO> GetAllCharacters()
        {
            List<Character> listC = new List<Character>();
            List<CharacterDTO> listCTDO = new List<CharacterDTO>();

            listC = TournamentManager.Instance.getCharacters().ToList();
            foreach(Character c in listC)
            {
                listCTDO.Add(new CharacterDTO(c));
            }

            return listCTDO;
        }

        [Route("{characterID:int}")]
        [HttpGet]
        public CharacterDTO GetCharacter(int characterID)
        {
            Character c = TournamentManager.Instance.getCharacterById(characterID);

            return new CharacterDTO(c);
        }


    [Route("")]
        [HttpPost]
        public CharacterDTO PostCharacter()
        {
            //string lala = Request.Form["bravoury"];
            //Console.WriteLine(Request.Form);
            //Character c = TournamentManager.Instance.createCharacter(character.FirstName, character.LastName);
            //return new CharacterDTO(c);
            return null;
        }
    }
}
