﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityLayer;
using WebApiGot.Models;
using BusinessLayer;

namespace WebApiGot.Controllers
{
    public class CharacterController : Controller
    {
        
        [HttpGet]
        public ActionResult Index()
        {

            List<Character> listC = new List<Character>();
            List<CharacterDTO> listCTDO = new List<CharacterDTO>();

            listC = TournamentManager.Instance.getCharacters().ToList();
            foreach (Character c in listC)
            {
                listCTDO.Add(new CharacterDTO(c));
            }

            return Json(listCTDO, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            Character c = TournamentManager.Instance.getCharacterById(id);
            
            //View("Details", new CharacterDTO(c));

            return Json(c, JsonRequestBehavior.AllowGet);
        }


        [HttpPost] //Advanced Rest Client
        public ActionResult Create(FormCollection collection)
        {
            return Json(new CharacterDTO(TournamentManager.Instance.getCharacterById(TournamentManager.Instance.createCharacter(Request.Form["firstname"], Request.Form["lastname"]).ID)), JsonRequestBehavior.AllowGet);
        }

        // POST: CharacterTest/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            if (Request.Form["firstname"] != null)
            {
                TournamentManager.Instance.setCharacterFirstName(id, Request.Form["firstname"]);
            }

            if (Request.Form["lastname"] != null)
            {
                TournamentManager.Instance.setCharacterLastName(id, Request.Form["lastname"]);
            }

            if (Request.Form["bravoury"] != null)
            {
                TournamentManager.Instance.setCharacterBravoury(id, Int32.Parse(Request.Form["bravoury"]));
            }

            if(Request.Form["strength"] != null)
            {
                TournamentManager.Instance.setCharacterStrength(id, Int32.Parse(Request.Form["strength"]));
            }

            if(Request.Form["health"] != null)
            {
                TournamentManager.Instance.setCharacterHealth(id, Int32.Parse(Request.Form["health"]));
            }

            if (Request.Form["crazyness"] != null)
            {
                TournamentManager.Instance.setCharacterCrazyness(id, Int32.Parse(Request.Form["crazyness"]));
            }

            return Json(new CharacterDTO(TournamentManager.Instance.getCharacterById(id)), JsonRequestBehavior.AllowGet);
        }

        // GET: CharacterTest/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CharacterTest/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
