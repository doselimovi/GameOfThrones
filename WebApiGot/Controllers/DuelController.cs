﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityLayer;
using WebApiGot.Models;
using BusinessLayer;

namespace WebApiGot.Controllers
{
    public class DuelController : Controller
    {
        // GET: Duel
        public ActionResult Index()
        {
            List<Duel> listD = new List<Duel>();
            List<DuelDTO> listDTDO = new List<DuelDTO>();

            listD = TournamentManager.Instance.getNewDuels();
            System.Diagnostics.Debug.WriteLine(listD.Count);
            foreach (Duel d in listD)
            {
                listDTDO.Add(new DuelDTO(d));
            }

            return Json(listDTDO, JsonRequestBehavior.AllowGet);
        }

        // GET: Duel/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Duel/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Duel/Create/5
        [HttpPost]
        public ActionResult Create(int id, FormCollection collection)
        {
            float solde = TournamentManager.Instance.getResultOfDuel(id, Int32.Parse(Request.Form["choice"]), Int32.Parse(Request.Form["mise"]));
            return View(solde);
        }

        // GET: Duel/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Duel/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Duel/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Duel/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
