﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer;

namespace WebApiGot.Controllers
{
    public class SoldeController : Controller
    {
        // GET: Solde
        public ActionResult Index()
        {
            return Json(TournamentManager.Instance.getSolde(), JsonRequestBehavior.AllowGet);
        }

        // GET: Solde/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Solde/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Solde/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Solde/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Solde/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Solde/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Solde/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
