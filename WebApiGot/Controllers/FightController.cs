﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using BusinessLayer;
using System.Web.Http;
using WebApiGot.Models;
using EntityLayer;

namespace WebApiGot.Controllers
{
    [RoutePrefix("api/fights")]
    public class FightController : ApiController
    {
        [Route("{fightID:int}")]
        [HttpGet]
        public FightDTO GetCharacter(int fightID)
        {
            Fight f = TournamentManager.Instance.getFightById(fightID);

            return new FightDTO(f);
        }

    }
}
