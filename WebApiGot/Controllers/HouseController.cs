﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;
using WebApiGot.Models;
using BusinessLayer;
using System.Web.Mvc;

namespace WebApiGot.Controllers
{
    public class HouseController : Controller
    {
        // GET: House
        public ActionResult Index()
        {
            List<House> listH = new List<House>();
            List<HouseDTO> listHTDO = new List<HouseDTO>();

            listH = TournamentManager.Instance.getHouses().ToList();
            foreach (House h in listH)
            {
                listHTDO.Add(new HouseDTO(h));
            }

            return Json(listHTDO, JsonRequestBehavior.AllowGet);
        }

        // GET: House/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: House/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: House/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: House/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: House/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: House/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: House/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
