﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiGot.Models;

namespace WebApiGot.Controllers
{
    public class TerritoryController : ApiController
    {
        public List<TerritoryDTO> GetAllTerritory()
        {
            List<TerritoryDTO> list = new List<TerritoryDTO>();

            TerritoryDTO dt = new TerritoryDTO();
            dt.EntityObject = 51;
            dt.OwnerId = 42;
            dt.Type = 69;
            list.Add(dt);

            return list;
        }
    }
}
