﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;

namespace WebApiGot.Models
{
    public class DuelDTO
    {
        public int id { get; set; }
        public Character id1 { get; set; }
        public Character id2 { get; set; }
        public float cote1 { get; set; }
        public float cote2 { get; set; }

        public DuelDTO(Duel d)
        {
            id = d.id;
            id1 = d.character1;
            id2 = d.character2;
            cote1 = d.cote1;
            cote2 = d.cote2;
        }
    }
}