﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;
using System.Runtime.Serialization;

namespace WebApiGot.Models
{
 
    [Serializable]
    public class CharacterDTO
    {
        private int id;
        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private int bravoury;
        public int Bravoury
        {
            get { return bravoury; }
            set { bravoury = value; }
        }
        private int crazyness;
        public int Crazyness
        {
            get { return crazyness; }
            set { crazyness = value; }
        }
        private int strength;
        public int Strength
        {
            get { return strength; }
            set { strength = value; }
        }
        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        private int health;
        public int Health
        {
            get { return health; }
            set { health = value; }
        }
        private Dictionary<int, RelationshipEnum> relationships;
        public Dictionary<int, RelationshipEnum> Relationships
        {
            get { return relationships; }
            set { relationships = value; }
        }

        public CharacterDTO(Character c)
        {
            id = c.ID;
            bravoury = c.bravoury;
            crazyness = c.crazyness;
            strength = c.strength;
            firstName = c.firstName;
            lastName = c.lastName;
            health = c.health;
            relationships = c.relationships;
        }
    }
}