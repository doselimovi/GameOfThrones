﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;

namespace WebApiGot.Models
{
    public class FightDTO
    {
        public int houseChallenger1 { get; set; }
        public int houseChallenger2 { get; set; }
        public int winningHouse { get ; set; }//0 si aucun, 1 ou 2 sinon

        public FightDTO(Fight f)
        {
            houseChallenger1 = f.houseChallenger1;
            houseChallenger2 = f.houseChallenger2;
            winningHouse = f.winningHouse;
        }
    }
}