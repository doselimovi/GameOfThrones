﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityLayer;

namespace WebApiGot.Models
{
    public class HouseDTO
    {
        public int NumberOfUnits { get; set; }
        public string Name { get; set; }

        public HouseDTO(House h)
        {
            NumberOfUnits = h.numberOfUnities;
            Name = h.name;
        }
    }
}