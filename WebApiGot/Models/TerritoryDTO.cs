﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiGot.Models
{
    public class TerritoryDTO
    {
        public int EntityObject { get; set; }

        public int OwnerId { get; set; }

        public int Type { get; set; }

        public TerritoryDTO()
        { }
    }
}