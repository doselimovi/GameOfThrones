﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApiGot
{
    public static class WebApiConfig
    {
        public static void Register(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "DefaultApi",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "CharacterController", action = "Details", id = UrlParameter.Optional }
            );
        }
    }
}
